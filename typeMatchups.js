import React from 'react';
import Type from './type';
import _ from 'lodash';

const colsStyle = {
  display: 'flex',
  justifyContent: 'center',
  minHeight: '300px',
};

const matchupStyle = {
  minWidth: '200px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-start',
  flexDirection: 'column',
  padding: '10px',
};

const matchHeadStyle = {
  margin: 0,
};

const matchFooterStyle = {
  marginBottem: '10px',
  justifyContent: 'center',
  display: 'flex',
  fontSize: '15px',
  fontWeight: 'bold',
};

const typeList = [
  'Fire',
  'Water',
  'Electric',
  'Grass',
  'Ice',
  'Fighting',
  'Poison',
  'Ground',
  'Flying',
  'Psychic',
  'Bug',
  'Rock',
  'Ghost',
  'Dragon',
  'Dark',
  'Steel',
  'Fairy',
];

/*eslint-disable */
const matchups = {
  Normal: { i: ['Ghost'], 2: ['Fighting'], 0.5: [] },
  Fire: { i: [], 2: ['Water', 'Ground', 'Rock'], 0.5: ['Fire', 'Grass', 'Ice', 'Bug', 'Steel', 'Fairy'] },
  Water: { i: [], 2: ['Grass', 'Electric'], 0.5: ['Fire', 'Water', 'Ice', 'Steel'] },
  Electric: { i: [], 2: ['Ground'], 0.5: ['Electric', 'Flying'] },
  Grass: { i: [], 2: ['Fire', 'Ice', 'Poison', 'Flying', 'Bug'], 0.5: ['Water', 'Grass', 'Electric', 'Ground'] },
  Ice: { i: [], 2: ['Fire', 'Fighting', 'Rock', 'Steel'], 0.5: ['Ice'] },
  Fighting: { i: [], 2: ['Flying', 'Psychic', 'Fairy'], 0.5: ['Bug', 'Rock', 'Dark'] },
  Poison: { i: [], 2: ['Ground', 'Psychic'], 0.5: ['Grass', 'Fighting', 'Poison', 'Bug', 'Fairy'] },
  Ground: { i: ['Electric'], 2: ['Water', 'Grass', 'Ice'], 0.5: ['Poison', 'Rock'] },
  Flying: { i: ['Ground'], 2: ['Electric', 'Ice', 'Rock'], 0.5: ['Grass', 'Fighting', 'Bug'] },
  Psychic: { i: [], 2: ['Bug', 'Ghost', 'Dark'], 0.5: ['Psychic', 'Fighting'] },
  Bug: { i: [], 2: ['Fire', 'Flying', 'Rock'], 0.5: ['Grass', 'Fighting', 'Ground'] },
  Rock: { i: [], 2: ['Water', 'Grass', 'Fighting', 'Ground', 'Steel'], 0.5: ['Normal', 'Fire', 'Poison', 'Flying'] },
  Ghost: { i: ['Normal', 'Fighting'], 2: ['Ghost', 'Dark'], 0.5: ['Poison', 'Bug'] },
  Dragon: { i: [], 2: ['Ice', 'Dragon', 'Fairy'], 0.5: ['Fire', 'Water', 'Grass', 'Electric', ] },
  Dark: { i: ['Psychic'], 2: ['Fighting', 'Bug', 'Fairy'], 0.5: ['Ghost', 'Dark'] },
  Steel: { i: ['Poison'], 2: ['Fire', 'Fighting', 'Ground'], 0.5: ['Normal', 'Grass', 'Ice', 'Flying', 'Psychic', 'Bug', 'Rock', 'Dragon', 'Steel', 'Fairy'] },
  Fairy: { i: ['Dragon'], 2: ['Poison', 'Steel'], 0.5: ['Bug', 'Dark', 'Fighting'] },
};
/*eslint-enable */

const TypeMatchups = function TypeMatchups(props) {
  const match = _.cloneDeep(matchups[props.data.types.t1]);

  if (props.data.types.t2) {
    const m2 = _.cloneDeep(matchups[props.data.types.t2]);

    match.i = _.union(match.i, m2.i);
    match[2] = _.difference(match[2], match.i);
    match[0.5] = _.difference(match[0.5], match.i);

    match[4] = _.intersection(match[2], m2[2]);
    match[0.25] = _.intersection(match[0.5], m2[0.5]);

    match[2] = _.xor(match[2], m2[2]);
    match[0.5] = _.xor(match[0.5], m2[0.5]);

    match[0] = _.intersection(match[2], match[0.5]);
    match[2] = _.difference(match[2], match[0]);
    match[0.5] = _.difference(match[0.5], match[0]);
  } else {
    match[4] = [];
    match[0.25] = [];
  }

  match[0] = _.difference(typeList,
    _.concat(match[4], match[2], match[0.5], match[0.25], match.i)
  );

  let imm = null;
  if (match.i.length > 0) {
    imm = (<div style={matchFooterStyle}>
      <span style={matchHeadStyle}>Immune to: {_.map(match.i, (type) =>
        <Type key={props.data.id + type} data={type} />)}
      </span>
    </div>);
  }

  return (
    <div>
      <div style={colsStyle}>
        <div style={matchupStyle}>
          <h2 style={matchHeadStyle}>1/4x</h2>
          {_.map(match[0.25], (type) => <Type key={props.data.id + type} data={type} />)}
        </div>
        <div style={matchupStyle}>
          <h2 style={matchHeadStyle}>1/2x</h2>
          {_.map(match[0.5], (type) => <Type key={props.data.id + type} data={type} />)}
        </div>
        <div style={matchupStyle}>
          <h2 style={matchHeadStyle}>Neutral</h2>
          {_.map(match[0], (type) => <Type key={props.data.id + type} data={type} />)}
        </div>
        <div style={matchupStyle}>
          <h2 style={matchHeadStyle}>2x</h2>
          {_.map(match[2], (type) => <Type key={props.data.id + type} data={type} />)}
        </div>
        <div style={matchupStyle}>
          <h2 style={matchHeadStyle}>4x</h2>
          {_.map(match[4], (type) => <Type key={props.data.id + type} data={type} />)}
        </div>
      </div>
      {imm}
    </div>
  );
};

TypeMatchups.propTypes = {
  data: React.PropTypes.object.isRequired,
};

export default TypeMatchups;
