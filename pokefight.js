import React from 'react';
import Header from './header';
import PokeList from './pokeList';

const pokemon = require('./data/pokemon.json');

const generalStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
};

export default class PokeFight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
    };
  }

  handleUserInput(filterText) {
    this.setState({ filterText });
  }

  render() {
    return (
      <div style={generalStyle}>
        <Header
          filterText={this.state.filterText}
          onUserInput={this.handleUserInput.bind(this)}
        />
        <PokeList
          data={pokemon}
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}
