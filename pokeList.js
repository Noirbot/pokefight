import React from 'react';
import Pokemon from './pokemon';
import _ from 'lodash';

const listStyle = {
  width: '100%',
  fontSize: '18px',
  display: 'flex',
  flexDirection: 'column',
};

export default class PokeList extends React.Component {
  constructor(props) {
    super(props);
    this.generateLine = this.generateLine.bind(this);
  }

  generateLine(poke) {
    if (poke.name.indexOf(this.props.filterText) !== -1) {
      return <Pokemon key={poke.id} data={poke} />;
    }

    return undefined;
  }

  render() {
    let pokemans = _.map(this.props.data, this.generateLine);

    return (
      <div style={listStyle}>
        {pokemans}
      </div>
    );
  }
}

PokeList.propTypes = {
  filterText: React.PropTypes.string.isRequired,
  data: React.PropTypes.array.isRequired,
};
