import React from 'react';
import Type from './type';

const Types = function Types(props) {
  if (props.data.t2) {
    return (
      <span>
        <Type data={props.data.t1} />
        <Type data={props.data.t2} />
      </span>
    );
  }

  return <Type data={props.data.t1} />;
};

Types.propTypes = {
  data: React.PropTypes.object.isRequired,
};

export default Types;
