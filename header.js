import React from 'react';

const searchStyle = {
  height: '50px',
  width: '33%',
  fontSize: '18px',
};

const headStyle = {
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  backgroundColor: '#d50000',
  paddingBottom: '20px',
  margin: 0,
};

const titleStyle = {
  fontSize: '30pt',
};

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.props.onUserInput(this.refs.filterTextInput.value);
  }

  render() {
    return (
      <div style={headStyle}>
        <h2 style={titleStyle}>PokèFight!</h2>
        <input
          type="text"
          placeholder="Pokèmon Name?"
          style={searchStyle}
          value={this.props.filterText}
          ref="filterTextInput"
          onChange={this.handleChange}
        ></input>
      </div>
    );
  }
}

Header.propTypes = {
  onUserInput: React.PropTypes.func.isRequired,
  filterText: React.PropTypes.string.isRequired,
};
