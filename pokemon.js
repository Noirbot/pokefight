import React from 'react';
import Types from './types';
import TypeMatchups from './typeMatchups';

const headerStyle = {
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
};

const nameStyle = {
  alignItems: 'center',
  margin: '10px 5px',
  borderRight: '2px solid black',
  paddingRight: '10px',
};

const pokemonStyle = {
  backgroundColor: '#aaaaaa',
};

const Pokemon = function Pokemon(props) {
  return (
    <div style={pokemonStyle}>
      <div style={headerStyle}>
        <h1 style={nameStyle}>{props.data.name}</h1>
        <Types data={props.data.types} />
      </div>
      <TypeMatchups data={props.data} />
    </div>
  );
};

Pokemon.propTypes = {
  data: React.PropTypes.object.isRequired,
};

export default Pokemon;
