import React from 'react';
import PokeFight from './pokeFight';

require('./styles/style.css');

export default class SimplePage {
  getBodyClasses() {
    return ['type', 'responsive'];
  }

  getElements() {
    return (
      <PokeFight />
    );
  }
}
